angular.module('app.controllers', [])

.controller('homeCtrl', ['$scope', '$stateParams',
function ($scope, $stateParams) {


}])

.controller('helpCtrl', ['$scope', '$stateParams',
function ($scope, $stateParams) {


}])

.controller('aboutCtrl', ['$scope', '$stateParams',
function ($scope, $stateParams) {


}])

.controller('allCtrl', ['$scope', '$stateParams',
function ($scope, $stateParams) {


}])

.controller('menuCtrl', ['$scope', '$stateParams',
function ($scope, $stateParams) {


}])


.controller('homeCtrl', function($scope, $timeout) {
    $scope.items =[{
      "id": "Item One",
      "point_lat": " -6.590083581632978",
      "point_lng": "110.66717147827148",
      "code": "10"

    }];
    var mapOptions = {
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    $scope.toggleGroup = function(group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
        for (var i = 0; i < $scope.items.length; i++) {
          var item = $scope.items[i];
          // search for opened item in the array of items
          if (item.id == group) { // found item
            // make latitude-longitude element


            var myLatlng = new google.maps.LatLng(item.point_lat, item.point_lng);

            // make map-element
            $scope.map = new google.maps.Map(document.getElementById('map' + item.code), mapOptions);

            // make marker-element
            var marker = new google.maps.Marker({
              position: myLatlng,
              map: $scope.map
            });

            // resize map now
            google.maps.event.trigger($scope.map, 'resize');
            $scope.map.setCenter(myLatlng);
            $scope.map.setZoom($scope.map.getZoom());

            // resize map when map is idle.
            google.maps.event.addListenerOnce($scope.map, 'idle', function() {
              console.log("map idled, resizing");
              google.maps.event.trigger($scope.map, 'resize');
              $scope.map.setCenter(myLatlng);
              $scope.map.setZoom($scope.map.getZoom());
            }); // shouldn't be making a function inside a loop but F it i'm trying everything,
            break;
          }
        }
        // resize map on time out (5 seconds)
        $timeout(function() {
          console.log("timeout, resizing");
          google.maps.event.trigger($scope.map, "resize");
          $scope.map.setCenter(myLatlng);
          $scope.map.setZoom($scope.map.getZoom());
        }, 5000);
      }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup === group;
    };
});
